/** 
 * MultiCast - Share values, not side effects
 * Subjects allow you broadcast values from a shared source, while limiting side effects to only 
 * one occurance. You start with a regular Observable, then multicast it to a Subject to be consumed 
 * by the end user. This magic happens because a single shared subscription is created to 
 * the underlying observable.
 */
const observable = Rx.Observable.fromEvent(document, 'click');

const clicks = observable
                 .do( _ => print('SIDE EFFECT!!') )

const subject = clicks.multicast(() => new Rx.Subject() );

const subA = subject.subscribe( c => print(`Sub A: ${c.timeStamp}`) )
const subB = subject.subscribe( c => print(`Sub B: ${c.timeStamp}`) )

subject.connect();
// SIDE EFFECT!!
// Sub A: 2687.62
// Sub B: 2687.62

// SIDE EFFECT!!
// Sub A: 4295.11
// Sub B: 4295.11