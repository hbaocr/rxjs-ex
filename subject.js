/**
 * Subject - An Observable that talks to subscribers
 * An RxJS Subject is just an Observable with the ability to call next() on itself to 
 * emit new values - in other words, it is an event emitter.
 */
const subject = new Rx.Subject()

const subA = subject.subscribe( val => print(`Sub A: ${val}`) )
const subB = subject.subscribe( val => print(`Sub B: ${val}`) )

subject.next('Hello')

let i=0;
setInterval(() => {
    subject.next('World'+i++);
}, 1000)

// Sub A: Hello
// Sub B: Hello
// Sub A: World
// Sub B: World