//There are times when you might only care about the first or last element of an Observable.

const names = Rx.Observable.of('Richard', 'Erlich', 'Dinesh', 'Gilfoyle')

names
  .first()
  .subscribe( n => print(n) )
// Richard


names
  .last()
  .subscribe( n => print(n) )
  // Gilfoyle
