/**
 * https://angularfirebase.com/lessons/rxjs-quickstart-with-20-examples/?source=post_page---------------------------#3-Unsubscribe-Turn-off-the-stream
 * This example to create the observer  from some popular source
 */


//1. from data 
const observable = Rx.Observable.create(observer => {
    observer.next('hello11');
    observer.next('hello12');

})

observable.subscribe(
    v => { print('next: ' + v) },
    e => { print('error: ' + e) },
    () => { print('complete') }
);




//2. from dom event
const clicks = Rx.Observable.fromEvent(document, 'click')
    .map(v => v)
    .filter(v => true)
    .subscribe(
        v => { print('dom next: ' + v) },
        e => { print('dom error: ' + e) },
        () => { print('dom complete') }
    );

//3. from Promise
const promise = new Promise((res, rej) => {
    setTimeout(() => {
        res('event from promise');
    })
})
const obsvPromise = Rx.Observable.fromPromise(promise);

obsvPromise.subscribe(
    v => { print('promise next: ' + v) },
    e => { print('promise error: ' + e) },
    () => { print('promise complete') }
);

//4. from timer

const obsvTimer = Rx.Observable.timer(1000);

obsvTimer.map(v => v)
    .filter(v => true)
    .subscribe(
        v => { print('timer next: ' + v) },
        e => { print('timer error: ' + e) },
        () => { print('timer complete') }
    );

// const obsvTimerInterval = Rx.Observable.interval(1000);

// let subcription=obsvTimerInterval.map(v => v)
// .filter(v => true)
// .subscribe(
//     v => { print('obsvTimerInterval next: '+v) },
//     e => { print('obsvTimerInterval error: '+e)},
//     () => { print('obsvTimerInterval complete') }
// );
//subscription.unsubscribe()



//5. from static data

const mashup = Rx.Observable.of('anything', ['you', 'want'], 23, true, { cool: 'stuff' })

mashup.map(v => v)
.filter(v => true)
.subscribe(
    v => { print('mashup next: ' + v) },
    e => { print('mashup error: ' + e) },
    () => { print('mashup complete') }
);
