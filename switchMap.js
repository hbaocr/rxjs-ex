/**
 * switchMap is commonly required when dealing with async data from a database or API call. 
 * For example, you need to get a user ID from an observable, then use it to query the database. 
 * In this example, we reset an interval after each mouse click.
 */
const clicks = Rx.Observable.fromEvent(document, 'click')


clicks.switchMap(click => {
    return Rx.Observable.interval(500)
})
.subscribe(i => print(i))