/**
 * Debounce and Throttle - Handle Stream Overload
 * Sometimes you might be dealing with a stream that that is emitting values crazy fast -
 * such as mouse move events in the browser. You might only want to handle these events 
 * every so often.
 *      1. Throttle - Give me the first value, then wait X time.
 *      2. Debounce - Wait X time, then give me the last value.
 */
const mouseEvents = Rx.Observable.fromEvent(document, 'mousemove')

mouseEvents
  .throttleTime(1000)
  .subscribe(v=>print(v))
// MouseEvent<data>
// wait 1 second...


// mouseEvents
//   .debounceTime(1000)
//   .subscribe()
// wait 1 second...
// MouseEvent<data>