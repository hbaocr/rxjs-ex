/**
 * Cold ===>
 * We know an Observable is cold if we subscribe at the same time, but get a different value.
 * They both subscribed to the same Observable with different results. 
 * This happens because the cold Observable doesn’t generate the random number
 * until after the subscription starts.
 */
let i=100;
const cold = Rx.Observable.create( (observer) => {
    
    observer.next( (i++) + Math.random() )
    observer.next( (i++) + Math.random() )
});

/**
 * Moi lan  cold sub la  observer.next chay 1 lan ( i tang)
 */
cold.subscribe(a => print(`Cold Subscriber A: ${a}`))
cold.subscribe(b => print(`Cold Subscriber B: ${b}`))
cold.subscribe(c => print(`Cold Subscriber C: ${c}`))


/**
 * Hot ==>
 * But how do we make an already cold observable hot? We can make a cold Observable hot
 * by simply calling publish() on it. This will allow the subscribers to share the same values
 * in realtime. To make it start emitting values, you call connect() after the subscription has started.
 */
const hot = cold.publish()

/**
 * du co bao nhieu lan hot sub,  observer.next chi chay 1 lan ( i tang 1)
 */
hot.subscribe(a => print(`Hot Subscriber A: ${a}`))
hot.subscribe(b => print(`Hot Subscriber B: ${b}`))
hot.subscribe(c => print(`Hot Subscriber C: ${c}`))

print("connect hot")
hot.connect()




